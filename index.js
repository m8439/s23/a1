//3
db.user.insertOne(
	{
		"name":"single",
		"accomodates": 2,
		"price": 1000,
		"description": "A simple room with all the basic necessities",
		"roomAvailable": 10,
		"isAvailable": false
	}
)

//4
db.user.insertMany([
	{
		"name":"double",
		"accomodates": 3,
		"price": 2000,
		"description": "A room fit for a small family goin on a vacation",
		"roomAvailable": 5,
		"isAvailable": false
	},
	{
		"name":"queen",
		"accomodates": 4,
		"price": 4000,
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"roomAvailable": 15,
		"isAvailable": false
	}
])

//5 
db.user.find(
	{
		"name": "queen"
	}
)

//6
db.user.updateOne(
	{
		"name":"queen"
	},
	{
		$set: {
			"roomAvailable": 0
		}
	}
)
//7
db.user.deleteMany(
	{ "roomAvailable": { $eq: 0 } }
)